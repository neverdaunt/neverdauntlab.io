## Meshes

`Dimensions` are how many positional units the particular mesh is, when the scale is set to `(1,1,1)`

* `pixel.tva` (deprecated)
    * Dimensions: 25x25x25
    * Has multiple bone points allowing non-cubic shapes
* `pixel2.tva`
    * Dimensions: 25x25x25
    * UV Mapped such that all sides display the texture prominently
* `tpixel.tva`
    * Dimensions: 10x10x10
    * UV Mapped such that the top and bottom have the full texture, and the sides are the edge-most pixel of the texture stretched between them
* `8bitNormish.tva`
    * Dimensions: ?x?x?
    * Used as the male player model, and can also be included in any object
    * Has many bones
* `8bitNormishete.tva`
    * Dimensions: ?x?x?
    * Used as the female player model, and can also be included in any object
    * Has many bones

There are other meshes not listed

## Structure

* [Stuff example](../examples/n8_stuff.md)
* [Hat example](../examples/n8_hat.md)
* [Weapon example](../examples/n8_hat_weapon.md)
* [Shield example](../examples/n8_hat_shield.md)
* [Monster example](../examples/n8_monster.md)

Example structure using `subpixelblue.png`:
```
臼NG

   
IHDR           szz・  ｣IDATX・`｣`撃Q0ﾔG襠碑g$ﾛ籥ﾞ|@・0・f$ﾙ<ｲﾏﾗ?ﾚI2悼dﾛ・・rB・;＝睇ｳ[$唄ﾝﾘ,#ﾇr・ﾐﾋ上ﾏnAｲ薊q ・E揵s・i窗 ﾙ・C・"・oｮ30,4ｧﾌ,ｲ菓n撃Q0
F:  -｢
ｿ    IENDｮB`・
StartData
New Thingy~Me~Stuff~
1
2
0
pixel2.tva
0:0.5019608:1:1/0:0:0:1/False
N8\square2.dds
 0: 0: 0
 0:-.7071068: 0: .7071068
 1: 1: 1
 1
Bone02
 0: 0: 0
 0: 0: 0: 1
 1.2: 1.2: 1.2
0
~
~1
```

1. First is the PNG data, which is the actual icon used in-game
1. After the PNG data, `StartData` is inserted on it's own line
1. The rest of the file is delimited by a `~` character into multiple sections
    1. Title
    1. Author
    1. Type of file
    1. Block Data (voxels/particles)
    1. Animations (only applicable for "monsters")
    1. Overall block scale

### Block Data

The first line is the total count of voxels (`number of voxels`) in this particular block. Then there is a repeating segment until `number of voxels` is reached:

1. `ID of voxel`
    * Unique ID and does not necessarily start at 1, and is not necessarily in order...
1. `ID of parent`
    * 0 = no parent
1. `mesh`
    * See meshes above for more information
1. material settings that are delimited by a `/` character
    1. `diffuse color`
    1. `emissive color`
    1. `shader type`
        * False
        * 1 = ?
        * 2 = team?
1. `texture`
    * Relative path from the textures directory
1. `position`
    * Vector3 (X,Y,Z)
1. `rotation`
    * Quaternion (Z,Y,X,W)
1. `overall scale of voxel`
    * this also would modify the positions and scales of child voxels if not set to `1:1:1`
1. repeating segment of the bones of current mesh
    * pixel.tva (has 2 bones)
        1. "Bone02"
        1. `Bone02 offset`
        1. `Bone02 rotation`
        1. `Scale of current bone`
        1. "Bone03"
        1. `Bone03 offset`
        1. `Bone03 rotation`
        1. `Scale of current bone`
    * pixel2.tva
        1. "Bone02"
        1. `Bone02 offset`
        1. `Bone02 rotation`
        1. `Scale of current voxel`
    * tpixel.tva
        1. "Bone02"
        1. `Bone02 offset`
        1. `Bone02 rotation`
        1. `Scale of current voxel`

Finally after reaching the end of `number of voxels`, the next line should be `number of particles`. If `number of particles` is not 0, repeat following segment until reaching the end:
 
1. `ID of Voxel parent`
1. Massive retarded line delimited by `:` on multiple levels
    1. `texture`
    1. `surface`
    1. `loop`
    1. `radius`
    1. `life`
    1. `speed`
    1. `weight`
    1. `direction.x`
    1. `direction.y`
    1. `direction.z`
    1. `random.x`
    1. `random.y`
    1. `random.z`
    1. `power`
    1. `Color1.R`
    1. `Color1.G`
    1. `Color1.B`
    1. `Color2.R`
    1. `Color2.G`
    1. `Color2.B`
    1. `Color3.R`
    1. `Color3.G`
    1. `Color3.B`
    1. `Color4.R`
    1. `Color4.G`
    1. `Color4.B`
    1. `Frame1.size`
    1. `Frame2.size`
    1. `Frame3.size`
    1. `Frame4.size`
    1. `Max Particles Emitted`
1. `position offset`
    * Vector3(x,y,z)
1. `rotation offset`
    * Quaternion(z,y,x,w)
1. `overall scale`
    * Vector3(x,y,z)

### Animations

I'll write this later or whatever

### Global Scale

Simply scales the entire object


## Converting Pixel to Pixel2

You can simply remove the entry for Bone03, then set the Y scale of Bone02 to the following formula:

```
1.0 - (bone02.position.y / 5.0 * 0.2) + (bone03.position.y / 5.0 * 0.2)
```
