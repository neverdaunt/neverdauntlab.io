# Tronic Specification

Note: Any node names like "True" or "False" are apparent bugs in the naming system.

## Tronic Node Spec

Name|Functionality
----|-------------
DataBlock | Read/Write Data
DataInA | Read Data
DataInB | Read Data
DataOutA | Write Data
DataOutB | Write Data
ChainIn | Function Arguments Input Data
ChainOut | Function Arguments Return Data
FlowIn | Flow Input
FlowOutA | Flow Output
FlowOutB | Flow Output

## Com Tronics

Tronics that do basic operator functionality or comparation.

### Data

* Internal name: cdata
* Model name: TronicData

Type | Display Name
-----|-------------
DataBlock | DataBlock

### Multi

* Internal name: cmulti
* Model name: TronicMultiply

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
DataOutA | C
FlowIn | Flow In
FlowOutA | Flow Out

### Plus

* Internal name: cplus
* Model name: TronicPlus

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
DataOutA | C
FlowIn | Float In
FlowOutA | Flow Out

### Minus

* Internal name: cminus
* Model name: TronicMinus

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
DataOutA | C
FlowIn | Float In
FlowOutA | Flow Out

### Div

* Internal name: cdivide
* Model name: TronicDivide

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
DataOutA | C
FlowIn | Float In
FlowOutA | Flow Out

### And

* Internal name: cand
* Model name: TronicAnd

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
DataOutA | Both
FlowIn | Float In
FlowOutA | Flow Out

### Random

* Internal name: crandom
* Model name: TronicRandom

Type | Display Name
-----|-------------
DataInA | Low
DataInB | High
DataOutA | Random Number
FlowIn | Float In
FlowOutA | Flow Out

### If>

* Internal name: cifgreat
* Model name: TronicIfGreater

Type | Display Name
-----|-------------
DataInA | Big Side
DataInB | Small Side
FlowIn | Float In
FlowOutA | is Greater
FlowOutB | is Not

### If=

* Internal name: cifequal
* Model name: TronicIfEqual

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
FlowIn | Float In
FlowOutA | Equal
FlowOutB | Not Equal

### Delay

* Internal name: cdelay
* Model name: TronicDelay

Type | Display Name
-----|-------------
DataInA | Seconds
FlowIn | Float In
FlowOutA | Flow Out

### Get

* Internal name: cGet
* Model name: TronicGet

Type | Display Name
-----|-------------
DataInA | Array
DataInB | Index
DataOutA | Value
FlowIn | Flow In
FlowOutA | Flow Out

### Remove

* Internal name: cRemove
* Model name: TronicRemove

Type | Display Name
-----|-------------
DataInA | Remove This
DataInB | From This
DataOutA | Data Out
FlowIn | FlowIn
FlowOutA | Flow Out

### IfContains

* Internal name: cIfContains
* Model name: TronicIfContains

Type | Display Name
-----|-------------
DataInA | Look In
DataInB | Look For
FlowIn | Flow In
FlowOutA | Its There
FlowOutB | No Luck

### Replace

* Internal name: cReplace
* Model name: TronicReplace

Type | Display Name
-----|-------------
DataInA | Old Value
DataInB | New Value
DataOutA | In this data
FlowIn | Flow In
FlowOutA | Flow Out

### Modulo

* Internal name: cModulos
* Model name: TronicModulo

Type | Display Name
-----|-------------
DataInA | A
DataInB | B
DataOutA | C
FlowIn | Flow In
FlowOutA | Flow Out

### Set

* Internal name: cSet
* Model name: TronicSet

Type | Display Name
-----|-------------
DataInA | Index
DataInB | Value
DataOutA | Array
FlowIn | Flow In
FlowOutA | Flow Out


## Tech Tronics

### Mover

* Internal name: tmover
* Model name: tronicmover

Type | Display Name
-----|-------------
DataInA | New Vector
DataOutA | Last Vector
FlowIn | Float In
FlowOutA | Flow Out

### Speaker

* Internal name: tspeaker
* Model name: TronicSpeaker

Type | Display Name
-----|-------------
DataInA | Say
FlowIn | Float In
FlowOutA | Flow Out

### Display

* Internal name: tdisplay
* Model name: TronicDisplay

Type | Display Name
-----|-------------
DataInA | False
FlowIn | Print
FlowOutA | Flow Out

### Coin Vend

* Internal name: tcoinvend
* Model name: TronicCoinVend

Type | Display Name
-----|-------------
DataInA | Amount to Drop
FlowIn | Float In
FlowOutA | Float Out
FlowOutB | Float Out

### RadioTransmit

* Internal name: tradiotransmiter
* Model name: TronicRadioTransmit

Type | Display Name
-----|-------------
DataInA | Channel
DataInB | Message
FlowIn | Float In

### Distance

* Internal name: cdistance
* Model name: TronicDistance

Type | Display Name
-----|-------------
DataInA | Vector A
DataInB | Vector B
DataOutA | Distance
FlowIn | Flow In
FlowOutA | Flow Out

### Animate

* Internal name: tAnimate
* Model name: tronicanimate

Type | Display Name
-----|-------------
FlowIn | Flow In
FlowOutA | Flow Out
DataInA | Animation Data
DataInB | Speed

### AnimateGet

* Internal name: tAnimateGet
* Model name: tronicanimateget

Type | Display Name
-----|-------------
DataOutA | Animation Data
FlowIn | Flow In
FlowOutA | Flow Out

### Time

* Internal name: cTime
* Model name: TronicTime

Type | Display Name
-----|-------------
DataInA | Type
DataOutA | Current Type
FlowIn | Flow In
FlowOutA | Flow Out


## Trigger Tronics

Tronics that initiate flow.

### Keyboard

* Internal name: rkeyboard
* Model name: TronicKeyboard

Type | Display Name
-----|-------------
DataOutA | False
FlowOutA | Typed

rswitch "Switch", "TronicRedSwitch"
FlowOutA | Flow Out

### RadioRecive

* Internal name: rradioreciver
* Model name: TronicRedSwitch

Type | Display Name
-----|-------------
DataInA | Channel
DataOutA | Message
DataOutB | Sender
FlowIn | Set Channel
FlowOutA | Flow Out

### Microphone

* Internal name: rmicrophone
* Model name: tronicmicrophone

Type | Display Name
-----|-------------
DataOutA | Heard
FlowOutA | Flow Out

### Proximity

* Internal name: rproximity
* Model name: TronicProximity

Type | Display Name
-----|-------------
FlowOutA | Flow Out

### Target

* Internal name: rtarget
* Model name: TronicTarget

Type | Display Name
-----|-------------
DataOutA | DMG
FlowOutA | Flow Out

### Rotor

* Internal name: troter
* Model name: tronicrotor

Type | Display Name
-----|-------------
DataInA | New Rotation
DataOutA | Last Rotation
FlowIn | Flow In
FlowOutA | Flow Out

### Switch

* Internal name: rblueswitch
* Model name: TronicBlueSwitch

Type | Display Name
-----|-------------
FlowOutA | Flow Out

### Switch

* Internal name: rgreenswitch
* Model name: TronicGreenSwitch

Type | Display Name
-----|-------------
FlowOutA | Flow Out

### Switch

* Internal name: ryellowswitch
* Model name: TronicYellowSwitch

Type | Display Name
-----|-------------
FlowOutA | Flow Out

### MiniTarget

* Internal name: rminitarget
* Model name: TronicMiniTarget

Type | Display Name
-----|-------------
DataOutA | DMG
FlowOutA | Flow Out

## Function Tronics

Tronics related to virtualized functions

### Flow Data

* Internal name: cfdat
* Model name: TronicFDat

Type | Display Name
-----|-------------
DataBlock | DataBlock

### Function Start

* Internal name: fStart
* Model name: TronicFunctionStart

Type | Display Name
-----|-------------
ChainIn | ChainIn
FlowOutA | Flow Out

### Function Call

* Internal name: fCall
* Model name: TronicFunctionCall

Type | Display Name
-----|-------------
ChainIn | ChainIn
ChainOut | ChainOut
FlowIn | Flow In
FlowOutA | Flow Out

### Data Chain

* Internal name: fChain
* Model name: TronicDataChain

Type | Display Name
-----|-------------
DataInA | True
ChainIn | ChainIn
ChainOut | ChainOut

### Function End

* Internal name: fEnd
* Model name: TronicFunctionEnd

Type | Display Name
-----|-------------
ChainOut | ChainOut
FlowIn | Flow In

### Function Call

* Internal name: fDataCall
* Model name: TronicFunctionCalldata

Type | Display Name
-----|-------------
DataInA | Function Name
ChainIn | ChainIn
ChainOut | ChainOut
FlowIn | Flow In
FlowOutA | Flow Out

## Ludus Tronics

Potentially dangerous tronics that were only allowed to be placed by Ludus (Engineers/Knights/Guards)

### Ludus Function Start

* Internal name: LfStart
* Model name: ludusfunction

Type | Display Name
-----|-------------
ChainIn | ChainIn
FlowOutA | Flow Out

### Flow Info

* Internal name: Linfo
* Model name: ludusfunction

Type | Display Name
-----|-------------
DataOutA | Array
FlowIn | Flow In
FlowOutA | Flow Out

### Give

* Internal name: Lgive/LEgive
* Model name: ludusfunction

Type | Display Name
-----|-------------
DataInA | Give Array
FlowIn | Flow In
FlowOutA | Flow Out

### GoFish

* Internal name: Lgofish
* Model name: ludusfunction

Type | Display Name
-----|-------------
DataInA | ID
DataInB | Value
DataOutA | Fish For
FlowIn | Flow In
FlowOutA | Go Fish
FlowOutB | Here Take it

### Hurty

* Internal name: tHurt
* Model name: ludusfunction

Type | Display Name
-----|-------------
DataInA | Size
FlowIn | Flow In
FlowOutA | Flow Out

### NPC Projector

* Internal name: tNPCProject
* Model name: ludusfunction

Type | Display Name
-----|-------------
DataInA | Look
DataInB | Say
FlowIn | Flow In
FlowOutA | Flow Out

### Deludus

* Internal name: LDeLudus
* Model name: ludusfunction

Type | Display Name
-----|-------------
FlowIn | Flow In
FlowOutA | Flow Out

### Flow Ludusification

* Internal name: LInLudus
* Model name: ludusfunction

Type | Display Name
-----|-------------
DataInA | Debug?
FlowIn | Flow In
FlowOutA | Flow Out

## Unimplemented Tronics

* csetval
* csplitnget
* tled
* texplosive
* tpush
* tpull
* tcannon
* cCount
* cContains
* cIndexof
